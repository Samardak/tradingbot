﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using Сonsumer;
using Сonsumer.IService;
using Сonsumer.IServices;
using Сonsumer.Service;
using Сonsumer.Services;

namespace Tests
{
    public class BaseTest
    {
        private ServiceCollection Services { get; set; }
        public IServiceProvider ServiceProvider { get; set; }

        public BaseTest()
        {
            Services = new ServiceCollection();

            Services.AddTransient<IUserRepository, UserRepository>();
            Services.AddTransient<IUserService, UserService>();

            var item = Options.Create(
                new AppSettings
                {
                    SQLConnection = "Server=hofqa.dublin.local;Database=facebook;Uid=root;Pwd=ASDcxz111;Maximum Pool Size=150;SslMode=none",
                    TestVariable = "test variable"
                });
            Services.AddSingleton(provider => item);



            ServiceProvider = Services.BuildServiceProvider();

        }
    }
}
